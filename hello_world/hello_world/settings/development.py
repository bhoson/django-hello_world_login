from .base import *

DEBUG = True

#DATABASES = {
#  'default': {
#  'ENGINE': 'django.db.backends.mysql',
#  'NAME': 'appdb',
#  'USER': 'appuser',
#  'PASSWORD': 'appuser@DB112358',
#  'HOST': '10.10.0.150',
#  'PORT': '3306',
#  'OPTIONS': {
#      'init_command': "SET sql_mode='STRICT_TRANS_TABLES',innodb_strict_mode=1",
#      'charset': 'utf8mb4',
#             },
#   }
#   }


DATABASES = {
  'default' : {},
  'app_db': {
  'ENGINE': 'django.db.backends.mysql',
  'NAME': 'appdb',
  'USER': 'appuser',
  'PASSWORD': 'appuser@DB112358',
  'HOST': '10.10.0.150',
  'PORT': '3306',
  'OPTIONS': {
      'init_command': "SET sql_mode='STRICT_TRANS_TABLES',innodb_strict_mode=1",
      'charset': 'utf8mb4',
             },
   },
  'social_db' : {
  'ENGINE': 'django.db.backends.postgresql',
  'NAME': 'socialdb',
  'USER': 'socialuser',
  'PASSWORD': 'socialuser@DB112358',
  'HOST': '10.10.0.198',
  'PORT': '5432',
   }
   }

DATABASE_ROUTERS = ['social.social_router.SocialRouter','login.login_router.LoginRouter']

#from mongoengine import *
#connect('social')


#Email settings
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST= 'smtp.gmail.com'
EMAIL_HOST_USER= 'some.mail.address@gmail.com'
EMAIL_HOST_PASSWORD= 'imaginary@@paas'
EMAIL_PORT= '587'
EMAIL_USE_TLS = True
