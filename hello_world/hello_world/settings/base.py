
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'k_z$^*5*8ya*2xj78(@2!dt^*(*&78z-o3o+&_$h4jt!vhn14l'


ALLOWED_HOSTS = ["*",]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'login.apps.LoginConfig',
    'social.apps.SocialConfig'
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'hello_world.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'hello_world.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "templates/scripts/css"),
    os.path.join(BASE_DIR, "templates/scripts/js"),
]

#STATIC_ROOT = "/var/www/hello_world/static/"


STATIC_ROOT = os.path.join(BASE_DIR, "static/")






LOGGING = {
  'version': 1,
  'disable_existing_loggers': False,
  'formatters' : {
    'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
  },
  'filters': {
    'require_debug_true': {
      '()': 'django.utils.log.RequireDebugTrue',
        },
    },
  'handlers': {
    'console': {
      'level': 'INFO',
      'filters': ['require_debug_true'],
      'class': 'logging.StreamHandler',
      'formatter': 'simple',
        },
    'mail_failure': {
      'level': 'ERROR',
      'class': 'django.utils.log.AdminEmailHandler',
        },
    'logfile': {
      'class':'logging.handlers.RotatingFileHandler',
      'filename': os.path.join(BASE_DIR, 'alllogs.log'),
      'maxBytes': 1024*1024*15,
      'backupCount': 2,
       },
    },
  'loggers': {
    'django': {
      'handlers': ['console','logfile'],
      'propagate': True,
        },
    'django.request': {
      'handlers': ['mail_failure','logfile'],
      'level': 'ERROR',
      'propagate': False,
        },
    }

}


