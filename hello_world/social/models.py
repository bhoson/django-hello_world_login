# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from cryptography.fernet import Fernet
from django.db import models

from mongoengine import *
connect(db='social',alias='user-db')
connect(db='esocial',alias='exec-db')


class Testimonial(Document):
  service_name = StringField(required=True,max_length=150)
  user_first_name = StringField(required=True,max_length=150)
  hash_tags = ListField(StringField(max_length=30))

  meta = {'db_alias':'user-db'}

class ExecTestimonial(Document):
  service_name = StringField(required=True,max_length=150)
  user_first_name = StringField(required=True,max_length=150)
  hash_tags = ListField(StringField(max_length=30))

  meta = {'db_alias':'exec-db'} 


TITLES = (('Mr','Mister'),
        ('Ms','Miss'))

class PrimeAppUser(models.Model):
  title = models.CharField(choices=TITLES,max_length=2)
  first_name = models.CharField(max_length = 150,null=True)
  middle_name = models.CharField(max_length = 150,null=True)
  last_name = models.CharField(max_length = 150,null=True)
  email = models.EmailField(max_length=50, null=False,unique=True)
  phone = models.CharField(max_length=15, null=False,unique=True)

  def __str__(self):
    return self.email + " " + self.phone

  class Meta:
    app_label = "social"



