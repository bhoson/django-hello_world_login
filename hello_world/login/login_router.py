class LoginRouter:
  """
    A router to control all database operations on models in the
    login application.
    """

  def db_for_read(self, model, **hints):
    if model._meta.app_label == 'login':
      return 'app_db'
    return None


  def db_for_write(self, model, **hints):
    if model._meta.app_label == 'login':
      return 'app_db'
    return None

  def allow_relation(self, obj1, obj2, **hints):
    if obj1._meta.app_label == 'login' or obj2._meta.app_label == 'login':
      return True
    return None

  def allow_migrate(self, db, app_label, model_name=None, **hints):
    if app_label == 'login':
      return db == 'app_db'
    return None


