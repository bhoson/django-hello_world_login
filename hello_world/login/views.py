# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse
from django.db import IntegrityError
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
from django.views.decorators.csrf import csrf_exempt
from lib.decorators import *
from django.core.mail import send_mail
from django.conf import settings
import logging
logger = logging.getLogger('django.request')

def send_email(password,email_to,subject=None,message=None):
  if subject == None and message == None:
    subject = "Confidential: Hello World"
    message = "Your password is : " + password
  status_msg = None
  try:
    status_msg = send_mail(subject,message,
	settings.EMAIL_HOST_USER,[email_to],fail_silently=False) 
  except Exception, e:
    logger.error('Mail could not be sent')
    print ("\n\n")
    print ("Exception in function: "+"send_email")
    print ("\nException info: ")
    print (e)
    return False
  return status_msg



@csrf_exempt
@request_intercept
def signup(request):
  if request.method == 'POST':
    first_name = request.POST.get("first_name")
    middle_name = request.POST.get("middle_name")
    last_name = request.POST.get("last_name")
    email = request.POST.get("email")
    phone = request.POST.get("phone")
    password = request.POST.get("password")
    
    new_user = AppUser(first_name=first_name,middle_name=middle_name,last_name=last_name,email=email,phone=phone,password=password)

    try:
      new_user.save()
      sent = send_email(new_user.decrypt_password(),new_user.email)
      if not sent:
        return JsonResponse({'success':True,'msg':'User created successfully,but mail could not be sent!'})
      else:
        return JsonResponse({'success':True,'msg':'User created successfully'})
    except IntegrityError, e:
      return JsonResponse({'success':False,'msg':e[1]}) 
    except Exception, e:
      return JsonResponse({'success':False,'msg':'Something went wrong! User could not be created.'})
  else:
    return JsonResponse({'success':False,'msg':'Hello there! Use POST request to create a user.'})




def user_login(request):
  form = LoginForm()
  if request.method == 'POST':
    form = LoginForm(request.POST)
    if form.is_valid():
      email = form.cleaned_data["email"]
      phone = form.cleaned_data["phone"]
      password = form.cleaned_data["password"]
      user = AppUser.objects.get(email=email,phone=phone)
      if password == user.decrypt_password():
        request.session["user_id"] = user.id
        return HttpResponseRedirect(reverse('login:home'))
      else:
        content = "<p> You fed wrong data mate! </p>"
        return HttpResponse(content)  
    else:
     form = LoginForm()
     return render(request, 'login/login.html',{'form':form})
  else:
    return render(request, 'login/login.html',{'form':form})



def home(request):
  if request.method == 'GET':
    if request.session.has_key("user_id"):
      return render(request, 'login/home.html') 
    else:
      content = "<p>First login, mate! </p>"
      return HttpResponse(content)
    

def logout(request):
  if request.method == 'GET':
    if request.session.has_key("user_id"):
      del request.session["user_id"]
      return HttpResponseRedirect(reverse('login:login'))
    else:
      content = "<p>First login, mate! </p>"
      return HttpResponse(content) 






