# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from cryptography.fernet import Fernet

from django.contrib.auth.models import User
# Create your models here.

TITLES = (('Mr','Mister'),
	('Ms','Miss')) 

class AppServiceManager(models.Model):
  manager = models.ForeignKey('ExecUser',on_delete=models.CASCADE)
  service = models.ForeignKey('AppService',on_delete=models.CASCADE)

  class Meta:
    app_label = "login"

class AppService(models.Model):
  service_name = models.CharField(max_length = 150)
  service_price = models.DecimalField(max_digits=5,decimal_places=2)

  class Meta:
    app_label = "login"

class AppUser(models.Model):
  title = models.CharField(choices=TITLES,max_length=2)
  first_name = models.CharField(max_length = 150,null=True)
  middle_name = models.CharField(max_length = 150,null=True)
  last_name = models.CharField(max_length = 150,null=True)
  email = models.EmailField(max_length=50, null=False,unique=True)
  phone = models.CharField(max_length=15, null=False,unique=True)
  created = models.DateTimeField(auto_now=True)
  password = models.CharField(max_length=120,null=False)
  key = models.CharField(max_length=44,null=True)
  subscriptions = models.ManyToManyField(AppService)

  def __str__(self):
    return self.email + " " + self.phone 
  def generate_key(self):
    self.key = Fernet.generate_key()
  def encrypt_password(self):
    f = Fernet(str(self.key))
    self.password = f.encrypt(str(self.password))
  def decrypt_password(self):
    f = Fernet(str(self.key))
    return f.decrypt(str(self.password))
  def save(self,*args,**kwargs):
    self.generate_key()
    self.encrypt_password()
    models.Model.save(self, *args, **kwargs)
  class Meta:
    db_table = "app_user"
    app_label = "login"


class ExecUser(models.Model):
  user = models.OneToOneField(AppUser,on_delete=models.CASCADE) 

  class Meta:
    app_label = "login"


#class UserModel(models.Model):
#  first_name = models.CharField(max_length=30,null=False,blank=False)
#  middle_name = models.CharField(max_length=30,null=False,blank=False)
#  last_name = models.CharField(max_length=30,null=False,blank=False)
#  email = models.EmailField(null=False,blank=False)


