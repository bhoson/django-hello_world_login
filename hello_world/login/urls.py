from django.conf.urls import url
from .views import *

app_name = 'login'

urlpatterns = [
    url(r'^signup/$', signup, name='signup'),
    url(r'^login/$', user_login, name='login'),
    url(r'^home/$', home, name="home"),
    url(r'^logout/$', logout, name="logout"),
]


