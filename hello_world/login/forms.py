
from django import forms
from django.forms import ModelForm


#class User(ModelForm):
#  class Meta:
#    model = UserModel
#    fields = ['first_name','middle_name','last_name','email'] 

class LoginForm(forms.Form):
  email = forms.CharField(label="Email",required=True,max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'email','placeholder':'eg. you@mailserver.com'}))
  phone = forms.CharField(label="Phone",required=True,max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'phone','placeholder':'eg. 9000000000'}))
  password = forms.CharField(label="Password",required=True,max_length=30,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))



