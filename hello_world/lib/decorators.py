
from functools import wraps
import json
import urllib
from django.http import QueryDict



def request_intercept(fn):
  @wraps(fn)
  def wrapper(request):
    try:
      if request.method == 'POST':
        q = QueryDict(urllib.urlencode(json.loads(request.body)))
        request.POST = q
      return fn(request)
    except Exception, e:
      print ("\n\n")
      print ("Exception while processing function: "+fn.__name__)
      print ("\nException info: ")
      print (e)
      return False
  return wrapper


